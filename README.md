## README
* Esse Projeto foi criado utilizando bedrock/sage.

## Dependências
* PHP 7.2
* Composer 1.9
* Node 10.16.3
* Yarn 1.21.1

## Instalação: (Raiz)
* Composer install na raiz
* Configure seu arquivo .env

## Instalação: (Tema)
* Acesse web/app/themes/rits
* composer install
* yarn install

## Banco de dados
* mysql

## Crie um Virtual Host:
* Ele deve apontar para a pasta web do repositório. 
* Adicione seu virtual host no arquivo .env